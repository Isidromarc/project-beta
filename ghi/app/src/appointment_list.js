import React, { useEffect, useState } from 'react';

function AppointmentList() {
  const [appointment, setAppointment] = useState([]);

  useEffect(() => {
    fetchAppointmentList();
  }, []);

  const fetchAppointmentList = async () => {
     
        const response = await fetch('http://localhost:8080/api/appointment/');
        if (response.ok){
            const data = await response.json();
            setAppointment(data.appointment.filter(appointment => appointment.status === "Created"));
        
        
        } else {
            console.error(response)
        }
     
  };
  const appointmentcancel = async (event) => {
    const response = await fetch(`http://localhost:8080/api/service/${event.target.value}/cancel`)
    if(response.ok){
      setAppointment(appointment.filter(appointment => appointment.id != event.target.value ))
    }
  }
  const appointmentfinished = async (event) => {
    const response = await fetch(`http://localhost:8080/api/service/${event.target.value}/finished`)
    if(response.ok){
      setAppointment(appointment.filter(appointment => appointment.id != event.target.value))
    }
  }
  

  return (
    <div>
      <h1>Appointment List</h1>
      <table className='table table-striped' >
        <thead>
          <tr>
            <th>Date and Time</th>
            <th>Reason</th>
            <th>Vin</th>
            <th>Is Vip?</th>
            <th>Customer</th>
            <th>Technician</th>
          </tr>
        </thead>
        <tbody>
          {appointment.map((appointment) => {
            return( 
            <tr key={appointment.id}>
              <td>{appointment.date_time}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.is_vip}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td><button onClick={appointmentcancel}value={appointment.id}>Cancel</button> <button onClick={appointmentfinished}value={appointment.id}>Finish</button> </td>
              
            </tr>
            )
})} 
        </tbody>
      </table>
    </div>
  );
};

export default AppointmentList;
