import React, { useState } from 'react';

const TechForm = () => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: ''
  });

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    
      const response = await fetch('http://localhost:8080/api/techs/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });
      if (response.ok){
        setFormData({ first_name: '', last_name: '', employee_id: '' });
        document.getElementById("Create-Technician-Form").reset()
      
      }


    };
  return (
    <div>
      <h1>Technician Account Creation</h1>
      <h2>Add a Technician</h2>
      <form onSubmit={handleFormSubmit} id='Create-Technician-Form'>
        <div>
          <label>
            First Name:
            <input
              type="text"
              name="first_name"
              value={formData.first_name}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
          <label>
            Last Name:
            <input
              type="text"
              name="last_name"
              value={formData.last_name}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
          <label>
            Employee ID:
            <input
              type="text"
              name="employee_id"
              value={formData.employee_id}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
          <button type="submit">Add Technician</button>
        </div>
      </form>
    </div>
  );
};

export default TechForm;
