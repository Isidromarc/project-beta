export {default as CustomerForm} from './forms/CustomerForm';
export {default as CustomerList} from './lists/CustomerList';
export {default as SaleForm} from './forms/SaleForm';
export {default as SalesList} from './lists/SalesList';
export {default as SalespeopleList} from './lists/SalespeopleList';
export {default as SalespersonForm} from './forms/SalespersonForm';
export {default as SalespersonHistory} from './lists/SalespersonHistory';
