import {useState} from 'react';

function CustomerForm(prop){
    const [formData, setFormData] = useState({
        "first_name" : "",
        "last_name" : "",
        "phone_number" : "",
        "address" : ""
    })

    // classNames for button loading on submit. submit button disappears
    // to prevent multiple submittions while processing fetch.
    const [spinnerClass, setSpinner] = useState("d-none")
    const [buttonClass, setButtonClass] = useState("")

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.id] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        setButtonClass("d-none")
        setSpinner("spinner-border text-dark")

        const postURL = "http://localhost:8090/api/customers/"
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const postResponse = await fetch(postURL, fetchOptions)
        if (postResponse.ok){
            setFormData({
                "first_name" : "",
                "last_name" : "",
                "phone_number" : "",
                "address" : ""
            })
        }

        // handle error mainly for phone-number. As there are different ways to put
        // a phone number in, (ie (555)555-5555 / 555 555-5555 / 555-555-555 / (555) 555-5555)
        // setting char limit on phone number input is not a proper solution, so Customer Form
        // frontend will piggy-back off of the custom error handler in the backend for phone Number
        // to display bad input errors. To test, submit an invalid phone number

        else {
            // use error message from the backend. backend error responses have a keys "error" and "msg"
            // in the body. Errors not from backend will throw a KeyError when trying to access .msg
            // and proceed to the catch block.
            try {
                const data = await postResponse.json();
                alert(`Error : ${JSON.stringify(data.msg)}`)
            }
            // for all other errors not handled by the backend. Serves generic error message.
            catch (e) {
                alert(`${postResponse.status} : ${postResponse.statusText}`)
            }
        }
        setSpinner("d-none")
        setButtonClass("")
    }

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Add a Customer</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.first_name} type="text" required id="first_name" placeholder="First Name" />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.last_name} type="text" required id="last_name" placeholder="Last Name" />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.phone_number} type="text" required id="phone_number" placeholder="000-000-0000" />
                    <label htmlFor="phone_number">Phone Number</label>
                </div>
                <div className="mb-3 text-start">
                    <label className="ms-1 fw-bold" htmlFor="address">Address:</label>
                    <textarea className="form-control" onChange={handleChange} value={formData.address} required id="address" maxLength="150" rows="3"/>
                </div>
                <div className={spinnerClass} role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <button className={buttonClass}>Submit</button>
            </form>
        </div>
    )
}

export default CustomerForm;
