import {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function SaleForm(prop){
    const navigate = useNavigate();
    const [autos, setAutos] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        "automobile": "",
        "salesperson": "",
        "customer": "",
        "price": ""
    })

    // classNames for button loading on submit. submit button disappears
    // to prevent multiple submittions while processing fetch(S).
    const [spinnerClass, setSpinner] = useState("d-none")
    const [buttonClass, setButtonClass] = useState("")

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.id] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault();
        setButtonClass("d-none")
        setSpinner("spinner-border text-dark")

        const postURL = "http://localhost:8090/api/sales/";
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type":"application/json"
            }
        }

        const postResponse = await fetch(postURL, fetchOptions);
        if (postResponse.ok){
            const newSale = await postResponse.json();
            setFormData({
                "automobile": "",
                "salesperson": "",
                "customer": "",
                "price": ""
            })

            const soldAutoURL = `http://localhost:8100${newSale.automobile.import_href}`
            const fetchOptions = {
                method: "put",
                body: JSON.stringify({"sold":true}),
                headers: {
                    "Content-Type":"application/json"
                }
            }

            // try-catch block to catch ERR_CONNECTION_REFUSED and other "no-response" errors.
            // Without try-catch block, fetch throws an uncaught promise error and doesn't
            // proceed to the if/else statement below.
            let updateResponse
            try {
                updateResponse = await fetch(soldAutoURL, fetchOptions)
            } catch {
                updateResponse = {"ok":false}
            }

            if (updateResponse.ok){
                navigate("/sales")
            }

            // if updating the vehicle to sold=True in the inventory API fails and we've created a Sale object
            // on the sales_api, we need to delete that sale object and show that an error
            // occured trying to complete the sale.
            else {
                const deleteSaleURL = `http://localhost:8090/api/sales/${newSale.id}/`
                const deleteResponse = await fetch(deleteSaleURL, {method:"DELETE"})
                if (!deleteResponse.ok){
                    alert("Could not delete invalid sale")
                }
                alert("Invalid sale, could not complete in inventory_api")
                setSpinner("d-none")
                setButtonClass("")
            }
        }

        // Error handling for post response to sales_api.
        else {
            // use error message from the backend. backend error responses have a key called "error"
            // and "msg" in the body. Errors not explicityly handled by the backend will throw a
            // KeyError when trying to access .msg and proceed to the catch block.
            try {
                const data = await postResponse.json();
                alert(`Error : ${JSON.stringify(data.msg)}`)
            }
            // for all other errors not handled by the backend. Serves generic error message.
            catch (e) {
                alert(`${postResponse.status} : ${postResponse.statusText}`)
            }
        }
    }

    const getAutos = async () => {
        const autosURL = 'http://localhost:8100/api/automobiles/';
        const autosResponse = await fetch(autosURL);
        if (autosResponse.ok){
            const data = await autosResponse.json();
            setAutos(data.autos.filter(auto => !auto.sold ))
        } else (
            alert("Failed to fetch automobiles")
        )
    }

    const getSalespeople = async () => {
        const salespeopleURL = "http://localhost:8090/api/salespeople/";
        const salespeopleResponse = await fetch(salespeopleURL);
        if (salespeopleResponse.ok){
            const data = await salespeopleResponse.json()
            setSalespeople(data.salespeople)
        } else (
            alert("Failed to fetch salespeople")
        )
    }

    const getCustomers = async () => {
        const customersURL = "http://localhost:8090/api/customers/";
        const customerResponse = await fetch(customersURL);
        if (customerResponse.ok){
            const data = await customerResponse.json();
            setCustomers(data.customers)
        } else (
            alert("Failed to fetch customers")
        )
    }

    useEffect(() => {
        getAutos();
        getSalespeople();
        getCustomers();
    }, []);

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Make a Sale</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select className="form-control" onChange={handleChange} value={formData.automobile} required id="automobile">
                        <option value="">Select an Automobile VIN</option>
                        {autos.map(auto => {
                            return (
                                <option value={auto.vin} key={auto.vin}>
                                    {auto.vin}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select className="form-control" onChange={handleChange} value={formData.salesperson} required id="salesperson">
                        <option value="">Select a Sales Representative</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option value={salesperson.employee_id} key={salesperson.employee_id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select className="form-control" onChange={handleChange} value={formData.customer} required id="customer">
                        <option value="">Select a Customer</option>
                        {customers.map(customer => {
                            return (
                                <option value={customer.id} key={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3 form-floating">
                    <input
                        onChange={handleChange}
                        className="form-control"
                        value={formData.price}
                        type="number"
                        step="0.01"
                        min="0.01"
                        max="10000000000"
                        required
                        id="price"
                        placeholder="1000.00"
                    />
                    <label htmlFor="price">Price</label>
                </div>
                <div className={spinnerClass} role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <button className={buttonClass}>Submit</button>
            </form>
        </div>
    )
}

export default SaleForm;
