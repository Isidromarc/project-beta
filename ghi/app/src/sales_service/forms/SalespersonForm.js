import {useState} from 'react';

function SalespersonForm(props){
    const [formData, setFormData] = useState({
        "first_name" : "",
        "last_name" : "",
        "employee_id": ""
    })

    // classNames for button loading on submit. submit button disappears
    // to prevent multiple submittions while processing fetch
    const [spinnerClass, setSpinner] = useState("d-none")
    const [buttonClass, setButtonClass] = useState("")

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.id] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        setButtonClass("d-none")
        setSpinner("spinner-border text-dark")

        const postURL = "http://localhost:8090/api/salespeople/"
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(postURL, fetchOptions)
        if (response.ok){
            setFormData({
                "first_name" : "",
                "last_name" : "",
                "employee_id": ""
            })

        }

        // handle error mainly for employee_id. Will return a message if that employee id already
        // exists in the database as employee_id is set as unique.
        else {
            // use error message from the backend. backend error responses have keys "error" and "msg"
            // in the body. Errors not from backend will throw a KeyError when trying to access .msg
            // and proceed to the catch block.
            try {
                const data = await response.json();
                alert(`Error : ${JSON.stringify(data.msg)}`)
            }
            // for all other errors not handled by the backend. Serves generic error message.
            catch (e) {
                alert(`${response.status} : ${response.statusText}`)
            }
        }
        setSpinner("d-none")
        setButtonClass("")
    }

    return(
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Register a Salesperson</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} type="text" value={formData.first_name} required id="first_name" placeholder="First Name" />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} type="text" value={formData.last_name} required id="last_name" placeholder="Last Name" />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} type="text" value={formData.employee_id} required id="employee_id" placeholder="Employee ID" />
                    <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button>Submit</button>
                <div className={spinnerClass} role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <button className={buttonClass}>Submit</button>
            </form>
        </div>
    )
}

export default SalespersonForm;
