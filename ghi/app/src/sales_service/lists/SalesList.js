import {useEffect, useState} from 'react';

function SalesList(prop){
    const [sales, setSales] = useState([])

    const getSales = async () => {
        const salesURL = "http://localhost:8090/api/sales/";
        const response = await fetch(salesURL);
        if (response.ok){
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        getSales();
    }, []);

    return (
        <>
            <h2 className="mt-3">Sales History</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale, index) => {
                        return (
                            <tr key={index}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.customer}</td>
                                <td>{sale.automobile}</td>
                                <td>{sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalesList;
