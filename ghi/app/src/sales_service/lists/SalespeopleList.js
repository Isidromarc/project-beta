import {useEffect, useState} from 'react';

function SalespeopleList(props){
    const [salespeople, setSalespeople] = useState([])

    const getPeople = async () => {
        const peopleURL = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(peopleURL)
        if (response.ok){
            const data = await response.json()
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
        getPeople();
    }, []);

    return (
        <>
            <h2 className="mt-3">Salespeople</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.employee_id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalespeopleList;
