import {useEffect, useState} from 'react';

function SalespersonHistory(props){
    const [sales, setSales] = useState([])
    const [displaySales, setDisplaySales] = useState([])
    const [salespeople, setSalespeople] = useState([])

    const handleChange = event => {
        setDisplaySales(sales.filter(sale => sale.salesperson.employee_id === event.target.value))
    }

    const getSales = async () => {
        const salesURL = "http://localhost:8090/api/sales/";
        const response = await fetch(salesURL)
        if (response.ok){
            const data = await response.json()
            setSales(data.sales)
        }
    }

    const getSalespeople = async () => {
        const salespeopleURL = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleURL);
        if (response.ok){
            const data = await response.json()
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
        getSales();
        getSalespeople();
    }, []);

    return (
        <>
            <h2 className="mt-3">Sales History</h2>
            <select onChange={handleChange} id="salesperson">
                <option value="">Select a Salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option value={salesperson.employee_id} key={salesperson.employee_id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    )
                })}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {displaySales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.customer}</td>
                                <td>{sale.automobile}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalespersonHistory;
