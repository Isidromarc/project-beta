import React, { useState, useEffect } from 'react';

const AppointmentForm = () => {
  const [formData, setFormData] = useState({
    date_time: '',
    reason: '',
    vin: '',
    customer: '',
    technician: ''
  });
  const [technicians, setTechnicians] = useState([])

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    
      const response = await fetch('http://localhost:8080/api/appointment/', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });
      if (response.ok){
        setFormData({ date_time: '', reason: '', vin: '', customer: '', technician: '' });
        document.getElementById("Create-Appointment-Form").reset()
      
      }
    };


    


    const fetchTechnicians = async () => {
        const techniciansURL = "http://localhost:8080/api/techs/";
        const techniciansResponse = await fetch(techniciansURL);
        if (techniciansResponse.ok){
            const data = await techniciansResponse.json();
            setTechnicians(data.techs);
            
        }
    }

    useEffect(() =>{
        fetchTechnicians();
    }, []);

  return (
    <div>
      <h1>Make an Appointment</h1>
      <h2>Details</h2>
      <form onSubmit={handleFormSubmit} id='Create-Appointment-Form'>
        <div className='col-auto'>
          <label>
            Date and Time 
            <input
              type="datetime-local"
              name="date_time"
              value={formData.date_time}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
          <label>
            Reason
            <input
              type="text"
              name="reason"
              value={formData.reason}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
          <label>
            Vin
            <input
              type="text"
              name="vin"
              value={formData.vin}
              onChange={handleInputChange}
              required={true}
            />
          </label>
        </div>
        <div>
        <label>
                Customer
            </label>
                <input
                type="text"
                name="customer"
                value={formData.customer}
                onChange={handleInputChange}
                required={true}
                />    
        </div>
            <select className='mb-3 form-control' onChange={handleInputChange} value={formData.technician} required={true} name='technician' >
                <option value=''>
                    Select a Technician
                </option>
                {technicians.map(technician =>{
                  return(
                    <option value={technician.id} key={technician.id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                    )
                  })}
            </select>
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default AppointmentForm;
