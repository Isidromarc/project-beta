import React, { useEffect, useState } from 'react';

function ServiceHistory() {
  const [appointment, setAppointment] = useState([])
  const [filterValue, setFilterValue] = useState("")
    
  useEffect(() => {
    fetchAppointmentList();
  }, []);

  const fetchAppointmentList = async () => {
     
        const response = await fetch('http://localhost:8080/api/appointment/');
        if (response.ok){
            const data = await response.json();
            setAppointment(data.appointment);

        
        } else {
            console.error(response)
        }
     
  };
  const handleFilterChange = (e) => {
    setFilterValue(e.target.value)
  }
  const getFilteredAppointments = () => {
    return appointment.filter(appointment =>
        appointment.vin.toLowerCase().includes(filterValue.toLowerCase())
        );
  }
  return (
    <div>
      <h1>Service History</h1>
        <input name="vin" onChange={handleFilterChange} placeholder='Your Vin #' />
      <table className='table table-striped' >
        <thead>
          <tr>
            <th>Date and Time</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Vin</th>
            <th>Is Vip?</th>
            <th>Customer</th>
            <th>Technician</th>
          </tr>
        </thead>
        <tbody>
          {getFilteredAppointments().map(appointment => {
            return( 
            <tr key={appointment.id}>
              <td>{appointment.date_time}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.is_vip}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              
            </tr>
            );
})} 
        </tbody>
      </table>
    </div>
  );
};

export default ServiceHistory;
