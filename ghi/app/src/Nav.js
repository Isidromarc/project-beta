import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className="nav-link" to="/techs/new"> Create a Technicians</NavLink>
            </li>
             <li className='nav-item'>
              <NavLink className="nav-link" to="/techs">List of Technicians</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/appointment">Appointments</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/appointment/new">Make a Appointment</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/service"> Service History</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Vehicles</a>
              <ul className="dropdown-menu">
                <li> <NavLink className="dropdown-item" to="manufacturers/">Manufacturers</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="manufacturers/new/">New Manufacturer</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="models/">Models</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="models/new/">New Model</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="automobiles/">Automobiles</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="automobiles/new/">New Automobile</NavLink> </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu">
                <li> <NavLink className="dropdown-item" to="sales/">Sales</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="sales/new/">New Sale</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="sales/history/">Sales History</NavLink> </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Salespeople</a>
              <ul className="dropdown-menu">
                <li> <NavLink className="dropdown-item" to="salespeople/">Salespeople</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="salespeople/new/">New Hire</NavLink> </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Customers</a>
              <ul className="dropdown-menu">
                <li> <NavLink className="dropdown-item" to="customers/">Customers</NavLink> </li>
                <li> <NavLink className="dropdown-item" to="customers/new">New Customer</NavLink> </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
