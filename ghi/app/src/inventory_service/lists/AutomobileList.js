import {useEffect, useState} from 'react';

function AutomobileList(prop){
    const [autos, setAutomobiles] = useState([])

    const getAutos = async () => {
        const autosURL = "http://localhost:8100/api/automobiles/";
        const response = await fetch(autosURL)
        if (response.ok){
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    }

    useEffect(() => {
        getAutos();
    }, [])

    return (
        <>
            <h2 className="mt-3">Automobile List</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.sold?"☑":"☐"}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default AutomobileList;
