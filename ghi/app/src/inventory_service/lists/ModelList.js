import {useEffect, useState} from 'react';

function ModelList(props){
    const [models, setModels] = useState([]);

    const getModels = async () => {
        const modelsURL = "http://localhost:8100/api/models/";
        const response = await fetch(modelsURL);
        if (response.ok){
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        getModels();
    }, []);

    return (
        <>
            <h2 className="mt-3">Models</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                    <img style={{height:"200px"}} src={model.picture_url} alt="picture goes here" />
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ModelList;
