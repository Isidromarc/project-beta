import {useEffect, useState} from 'react';

function ManufacturerList(props){
    const [manufacturers, setManufacturers] = useState([])

    const getManufacturers = async () => {
        const manufacturerURL = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerURL);
        if (response.ok){
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getManufacturers();
    }, [])

    return (
        <>
            <h2 className="mt-3">Manufacturers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ManufacturerList;
