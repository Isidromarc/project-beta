import {useState} from 'react';

function ManufacturerForm(props){
    const [manufacturer, setManufacturer] = useState("")

    const handleChange = event => {
        setManufacturer(event.target.value)
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const postURL = "http://localhost:8100/api/manufacturers/";
        const data = {
            "name":manufacturer
        }
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type":"application/json"
            }
        }

        const response = await fetch(postURL, fetchOptions)
        if (response.ok){
            setManufacturer("")
            alert("Post Successful")
        }
    }

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Add a Manufacturer</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={manufacturer} type="text" required id="name" placeholder="Manufacturer" />
                    <label htmlFor="name">Manufacturer</label>
                </div>
                <button>submit</button>
            </form>
        </div>
    )
}

export default ManufacturerForm;
