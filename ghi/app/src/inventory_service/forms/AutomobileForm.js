import {useEffect, useState} from 'react';

function AutomobileForm(prop){
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        "vin":"",
        "year":"",
        "color":"",
        "model_id":""
    })

    const getModels = async () => {
        const modelsURL = "http://localhost:8100/api/models/";
        const response = await fetch(modelsURL);
        if (response.ok){
            const data = await response.json()
            setModels(data.models)
        }
    }

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.name] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const postURL = "http://localhost:8100/api/automobiles/"
        const fetchOptions = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type":"application/json"
            }
        }
        const response = await fetch(postURL, fetchOptions)
        if (response.ok){
            setFormData({
                "vin":"",
                "year":"",
                "color":"",
                "model_id":""
            })
        }
    }

    useEffect(() => {
        getModels();
    }, []);

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Add an Automobile</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.vin} type="text" required id="vin" name="vin" placeholder="VIN" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.year} type="number" required id="year" name="year" placeholder="Year" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange} value={formData.color} type="text" required id="color" name="color" placeholder="Color" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                    <select className="form-control" onChange={handleChange} value={formData.model_id} id="model" name="model_id" required>
                        <option value="">Select a Model</option>
                        {models.map(model => {
                            return (
                                <option value={model.id} key={model.id}>
                                    {model.manufacturer.name} {model.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button>Submit</button>
            </form>
        </div>
    )
}

export default AutomobileForm;
