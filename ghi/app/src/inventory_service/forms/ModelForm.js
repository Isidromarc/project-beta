import {useEffect, useState} from 'react';

function ModelForm(prop) {
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState({
        "name": "",
        "picture_url":"",
        "manufacturer_id": ""
    })

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const postURL = "http://localhost:8100/api/models/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type":"application/json"
            }
        }

        const postResponse = await fetch(postURL, fetchOptions);
        if (postResponse.ok){
            setFormData({
                "name": "",
                "picture_url": "",
                "manufacturer_id": ""
            })
        }
    }

    const getManufacturers = async () => {
        const manURL = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manURL);
        if (response.ok){
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getManufacturers();
    } ,[]);

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h2 className="card-header">Add a Model</h2>
            <form className="card-body" onSubmit={handleSubmit}>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleFormChange} value={formData.name} type="text" required id="name" name="name" placeholder="Model Name" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleFormChange} value={formData.picture_url} type="url" required id="picture_url" name="picture_url" placeholder="Picture URL" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select className="form-control" onChange={handleFormChange} value={formData.manufacturer_id} id="manufacturer" name="manufacturer_id" required>
                        <option value="">Select a Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option value={manufacturer.id} key={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button>Submit</button>
            </form>
        </div>
    )
}

export default ModelForm;
