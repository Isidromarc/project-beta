import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './tech_form';
import TechList from './tech_list';
import AppointmentList from './appointment_list';
import AppointmentForm from './appointment_form';
import ServiceHistory from './service_history';
import { CustomerForm, CustomerList, SaleForm, SalesList,
  SalespeopleList, SalespersonForm, SalespersonHistory} from './sales_service/_components';
import {ManufacturerList, ManufacturerForm, ModelList,
  ModelForm, AutomobileList, AutomobileForm} from './inventory_service/_components';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="techs/">
            <Route path="new/" element={ <TechForm />} />
            <Route index element= {<TechList />} />
          </Route>
          <Route path="appointment/">
            <Route index element= {<AppointmentList />} />
            <Route path = "new/" element={<AppointmentForm />} />
          </Route>
          <Route path="service/" >
            <Route index element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturerList />} />
            <Route path="new/" element={<ManufacturerForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<ModelList />} />
            <Route path="new/" element={<ModelForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="salespeople/">
            <Route index element={<SalespeopleList />} />
            <Route path="new/" element={<SalespersonForm />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomerList />} />
            <Route path="new/" element={<CustomerForm />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="new/" element={<SaleForm />} />
            <Route path="history/" element={<SalespersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
