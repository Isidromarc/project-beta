# CarCar

Team:

* Myron - Automobile Service
* Marce Isidro - Automobile Sales Service

## Design
We used bootstrap for the designing of our website.
## Service microservice

I've created a technician model a automobilevo and an appoinment model with status choices. I also made functions to create a appointment for any services needed done to a vehicle also made a function to give vip status to any who purchases a vehicle from our inventory. I made a "service history" list to show all appointment regardless of where the vehicle was purchased.

## Sales microservice

Models: AutomobileVO, Salesperson, Customer, Sale

---AutomobileVO Properties---
import_href - string URL of Automobile Data in inventory API
vin - string VIN of vehicle
sold - Boolean

---Salesperson Properties---
first_name - string
last_name - string
employee_id - string unique ID for employee
current_hire - Boolean. Set to False if employee is "deleted" because he no longer works there.

---Customer Properties---
first_name - string
last_name - string
address - string
phone_number - string
active - Boolean. Set to False when the customer is "deleted"

---Sale Properties---
automobile - Foreign Key to AutomobileVO
salesperson - Foreign Key to Salesperson
customer - Foreign Key to Customer
price - Decimal Field. min-max set on frontend. Min= 0.01 Max=10,000,000,000.00

port: 8090
host_name: localhost
Database: remote Postgres

---END POINTS---
"GET"    | api/salespeople/                   | get a list of salespeople | required data: none
"POST"   | api/salespeople/                   | create a new salesperson  | required data: first_name, last_name, employee_id
"GET"    | api/salespeople/<int:employee_id>/ | get salesperson details   | required data: none
"DELETE" | api/salespeople/<int:employee_id>/ | delete salesperson        | required data: none
"GET"    | api/customers/                     | get a list of customers   | required data: none
"POST"   | api/customers/                     | create a new customer     | required data: first_name, last_name, address, phone_number
"GET"    | api/customers/<int:id>/            | get customer details      | required data: none
"DELETE" | api/customers/<int:id>/            | delete a customer         | required data: none
"GET"    | api/sales/                         | get list of sales         | required data: none
"POST"   | api/sales/                         | create a new sale         | required data: automobile, salesperson, customer, price
"GET"    | api/sales/<int:id>/                | get sale details          | required data: none
"DELETE" | api/sales/<int:id>/                | delete a sale             | required data: none
"DELETE" | api/sales/123AHDON'TUSETHISLINKUNLESSYUNOWATUDOING/2468/ | purges sales with empty reference key from database | required data: none

sales_api polls automobile data from inventory_api and saves them to AutomobileVO. AutomobileVO, Salesperson and Customer data
is preserved from deletion by setting their applicable property to False. This is to preserve Sales data as the Sale model refers
to those three models, and it doesn't make sense for a dealership to delete it's related sales history because a salesperson quit,
an automobile no longer is part of their inventory because of a sale or a Customer opts out of their application.

Customer model accepts different formats for the phone number, and normalizes them to 000-000-0000. Seeing as Sales data will never
trully be preserved perfectly, Sale has an endpoint to access it's class method that will delete all sales objects with empty references
values for its salesperson, automobileVO or customer property. Error handling happens in the views, and for errors that
are explicitly handled the front-end will receive a response body with the keys "error" containing a general description of the error and
"msg" containing a more detailed description of the message. "msg" may be in a dictionary format so stringifying the message on the front
end will display the entire message as a string.

In the event that the front-end displays sales data with an empty reference to its related models, the returned data for that attribute will be
the string "none" and will display nothing on the front end. This is handled by the custom encoder NoneTypeEncoder located in the common/json.py file.
DecimalEncoder handles the decoding of the price attribute for the Sale model.
