from django.urls import path
from service_rest.views import (technician_list, appointment_list, tech_detail,service_history,
appointment_canceled,appointment_finished, delete_appointment, service_detail)

urlpatterns = [
    path('techs/', technician_list, name= 'techs_list'),
    path('appointment/', appointment_list, name='appointments'),
    path('techs/<int:tech_id>/',tech_detail,name= 'tech-detail'),
    path('service/', service_history, name= 'service_history'),
    path('service/<int:id>/cancel/', appointment_canceled, name= "cancellation"),
    path('service/<int:id>/finished/', appointment_finished, name= 'finished'),
    path('appointment/<int:id>/', delete_appointment, name='delete_appointment'),
    path('servicehistory/<str:vin>/',service_detail, name='service_detail')
]
