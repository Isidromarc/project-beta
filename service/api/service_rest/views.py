from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from service_rest.models import Technician, Appointment
from common.json import TechnicianEncoder,AppointmentEncoder, AutomobileVO
import json




@require_http_methods(["GET", "POST", "DELETE"])
def technician_list(request):
    if request.method == 'GET':
        techs = Technician.objects.all()
        content = {
            "techs": techs
        }
        return JsonResponse(content, encoder=TechnicianEncoder)
    ####################END OF LIST
    elif request.method == "POST":
        techs = json.loads(request.body)
        required_properties = [
            "first_name",
            "last_name",
            "employee_id"
        ]
        for property in required_properties:
            try:
                techs[property]
            except KeyError:
                return JsonResponse(
                    {
                        "Error": "Missing one or more properties",
                        "Required properties":{
                            "first_name":"string",
                            "last_name":"string",
                            "employee_id":"string"
                        }
                    }, status=400
                )
        new_tech = Technician.objects.create(**techs)
        return JsonResponse(new_tech, TechnicianEncoder, safe=False)
    #############################END OF FORM
    else:
        techs = json.loads(request.body)

        try:
            employee_id = techs['id']
            count, _ = Technician.objects.filter(id=employee_id).delete()
            return JsonResponse({
                "Deleted": count > 0,
            })
        except KeyError:
            return JsonResponse(
                {
                    "error":"missing id property",
                    "id":"integer"
                }, status=400
            )
        

@require_http_methods(["GET" ,"POST"])
def appointment_list(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        content = {
            "appointment": appointments
        }
        return JsonResponse(content, encoder=AppointmentEncoder)
        ####################END OF LIST
    elif request.method == "POST":
        try:
            appointments = json.loads(request.body)
            required_properties = [
            "date_time",
            "reason",
            "vin",
            "customer",
            "technician"
        ]
            ################END OF FORM
            try:
                AutomobileVO.objects.get(vin=appointments["vin"], sold = True)
                appointments["is_vip"] = "yes"
            except AutomobileVO.DoesNotExist:
                appointments["is_vip"] = "no"
        ###############END OF VIP VERIFICATION    
            for property in required_properties:
                try:
                    appointments[property]
                except KeyError:
                    return JsonResponse(
                    {
                        "Error": "Missing one or more properties",
                        "Required properties": {
                            "date_time": "string",
                            "reason": "string",
                            "vin": "string",
                            "customer": "string",
                            "technician": "string"
                        }
                    }, status=400
                )
            technician_id = appointments.pop("technician")
            technician = Technician.objects.get(id=technician_id)
            new_appointment = Appointment.objects.create(technician=technician, **appointments)
            return JsonResponse(new_appointment,encoder=AppointmentEncoder, safe=False)
        except json.JSONDecodeError:
            return JsonResponse(
                {
                    "Error": "Invalid JSON payload"
                },
                status=400
            )
    else:
        return JsonResponse(
            {
                "Error": "Invald request method"
            },
            status=405
        )        
@require_http_methods(["GET"])
def tech_detail(request, tech_id=None):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=tech_id)
            return JsonResponse(tech, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse(
                {
                    "error":"Technician does not exist"
                }, status = 404
            )
@require_http_methods(["GET"])
def appointment_canceled(request, id):
    try:
        appointments = Appointment.objects.get(id=id)
        appointments.cancel_appointment()
        return JsonResponse(appointments, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
            return JsonResponse(
                {
                    "error":"Appointment does not exist"
                }, status = 404
                )    
                           
@require_http_methods(["GET"])
def appointment_finished(request, id):
    try:
        appointments = Appointment.objects.get(id=id)
        appointments.finish_appointment()
        return JsonResponse(appointments, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
            return JsonResponse(
                {
                    "error":"Appointment does not exist"
                }, status = 404
                )    

@require_http_methods(["GET", "POST"])
def service_history(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        content = {
            "service": appointments
        }
        return JsonResponse(content, encoder=AppointmentEncoder)
 

@require_http_methods(["GET"])
def service_detail(request,vin):
    if request.method == "GET":
        try:
            history = Appointment.objects.filter(vin=vin)
            return JsonResponse(history, encoder=AppointmentEncoder,safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {
                    "error": "Vin is invalid"
                }, status = 404
            )


@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return JsonResponse({"success": "Appointment deleted successfully"})
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment does not exist"}, status=404)
