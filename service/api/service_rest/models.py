from django.db import models




class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=12)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField()

class Appointment(models.Model):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('canceled', 'Canceled'),
        ('finished', 'Finished'),
    )

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=300)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Created')
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    is_vip = models.CharField(max_length=10, default="No")
    technician = models.ForeignKey('Technician', on_delete=models.CASCADE)

    def cancel_appointment(self):
        self.status = 'canceled'
        self.save()

    def finish_appointment(self):
        self.status = 'finished'
        self.save()

    
        
    
    
    
