import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO
# from sales_rest.models import Something


def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles")
            data = json.loads(response.content)

            for auto in data["autos"]:
                default = {
                    "import_href": auto["href"],
                    "vin": auto["vin"],
                    "sold": auto["sold"]
                }

                AutomobileVO.objects.update_or_create(
                    import_href = auto["href"],
                    defaults = default
                )
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
