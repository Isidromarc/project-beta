import json
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
from sales_rest.models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
from common.json import (
    SalespersonListEncoder,
    SalespersonDetailEncoder,
    CustomerListEncoder,
    CustomerDetailEncoder,
    SalesListEncoder,
    SaleDetailEncoder
)

@require_http_methods(["GET", "POST"])
def api_sales_persons(request):

    # GET method only gets currently hired employees -----------
    if request.method == "GET":
        employees = Salesperson.objects.filter(current_hire=True)
        data = {
            "salespeople":employees
        }
        return JsonResponse(data, encoder=SalespersonListEncoder)

    # POST method with error checks -----------------------------
    else:
        required_properties = {
            "first_name": "string max_length=30",
            "last_name": "string max_length=30",
            "employee_id": "string max_length=30"
        }

        # Check for empty data or non-standard-json data ---------
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({
                "error":"Empty or invalid json data",
                "msg": {"Required_Properties": required_properties}
            }, status=400 )

        # Check for missing properties. Normalization can
        # also happen during this process. ------------------------
        error = []
        for property in required_properties:
            try:
                data[property]
            except KeyError:
                error.append(property)

        if error:
            return JsonResponse({
                "error":"Missing one or more properties",
                "msg" : {"Missing Properties": error}
            }, status=400 )

        # Check for unnexpected data or error
        # when creating an employee ID That already exists. --------
        try:
            newPerson = Salesperson.objects.create(**data)
            return JsonResponse(newPerson, encoder=SalespersonDetailEncoder, safe=False)
        except TypeError:
            return JsonResponse({
                "error":"Unexpected Property",
                "msg": {"Assignable Properties": [
                    "first_name",
                    "last_name",
                    "employee_id",
                ]}
            }, status=400)
        except IntegrityError:
            return JsonResponse({
                "error":"Employee ID already Exists",
                "msg" : "Employee ID already Exists"
            }, status=400)


@require_http_methods(["GET","DELETE"])
def api_sales_person (request, employee_id):

    # Check if salesperson exists before routing
    # to "GET" or "DELETE" logic --------------------------------------
    try:
        sales_person = Salesperson.objects.get(employee_id=employee_id, current_hire=True)
    except Salesperson.DoesNotExist:
        return JsonResponse({
                "error":"Salesperson Does Not Exist",
                "msg" : "Salesperson Does Not Exist"
            }, status=404)

    # GET --------------------------------------------------------------
    if request.method == "GET":
        return JsonResponse(sales_person, encoder=SalespersonDetailEncoder, safe=False)

    # DELETE sets current_hire to False. See Salesperson Model ---------
    else:
        sales_person.current_hire = False
        sales_person.save()
        return JsonResponse({
            sales_person.employee_id: "No longer active"
        })


@require_http_methods(["GET","POST"])
def api_customers(request):

    # GET method only gets customers that are
    # "active" AKA haven't been "deleted" by the delete endpoint ----------
    if request.method == "GET":
        customers = Customer.objects.filter(active=True)
        data = {
            "customers": customers
        }
        return JsonResponse(data, encoder=CustomerListEncoder)

    # POST with error checks -----------------------------------------------
    else:
        required_properties = {
            "first_name": "string max_length=30",
            "last_name": "string max_length=30",
            "address": "string max_length=150",
            "phone_number": "string 10 digits"
        }

        # check for empty or non-standard JSON data ------------------------
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({
                "error" : "Empty or bad json data",
                "msg" : "Empty or bad json data"
            }, status=400)

        # Check for required properties. Normalize if it applies-------------
        error = []
        for property in required_properties:
            try:
                value = data[property]
                if property == "phone_number":
                    data[property] = Customer.normalize_phone_number(value)

            except KeyError:
                error.append(property)

            except Customer.BadInput as e:
                return JsonResponse({
                    "error":"Bad Input",
                    "msg" : str(e)
                }, status=400)

        if error:
            return JsonResponse({
                "error":"Missing one or more required properties",
                "msg" : {"Missing Properties": error}
            }, status=400)

        # Check for unexpected extra data ------------------------------------
        try:
            new_customer = Customer.objects.create(**data)
            return JsonResponse(new_customer, encoder=CustomerDetailEncoder, safe=False)

        except TypeError:
            return JsonResponse({
                "error":"Unexpected Properties",
                "msg" : {"Assignable Properties": required_properties}
            }, status=400)


@require_http_methods(["GET","DELETE"])
def api_customer(request, customer_id):

    # Check to see customer exists before routing to GET or DELETE ------------
    try:
        customer = Customer.objects.get(id=customer_id, active=True)

    except Customer.DoesNotExist:
        return JsonResponse({
            "error":"Customer ID does not exist",
            "msg" : "Customer ID does not exist"
        }, status=404)

    # GET ---------------------------------------------------------------------
    if request.method == "GET":
        return JsonResponse(customer, encoder=CustomerDetailEncoder, safe=False)

    # DELETE sets active to False. See Customer model -------------------------
    else:
        customer.active = False
        customer.save()
        return JsonResponse({
            "Deactivated":"True",
            "message":"Customer data will no longer appear in queries. Data will\
 be retained for sales archives.",
            "customer":customer
        }, encoder = CustomerDetailEncoder)


@require_http_methods(["GET", "POST"])
def api_sales(request):

    # GET ----------------------------------------------------------------------
    if request.method == "GET":
        sales = Sale.objects.all()
        data = {
            "sales":sales
        }
        return JsonResponse(data, encoder=SalesListEncoder)

    # POST with Error Checks ----------------------------------------------------
    else:
        required_properties = {
            "automobile": "string vin",
            "salesperson": "string employee id",
            "customer": "int customer id",
            "price": "positive number 0.01 to 10,000,000,000"
        }

        # Check for empty or non-standard JSON data -----------------------------
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({
                "error":"Empty or invalid json data",
                "msg" : {"Required Properties": required_properties}
            }, status=400)

        # Check for required properties -----------------------------------------
        error = []
        for property in required_properties:
            try:
                data[property]
            except KeyError:
                error.append(property)

        if error:
            return JsonResponse({
                "error":"Missing one or more required properties",
                "msg" : {"Missing Properties": error}
            }, status=400)

        # Catch DoesNotExist errors for auto, salesperson and customer ------------
        errors = {}
        try:
            auto = AutomobileVO.objects.get(vin = data["automobile"])
            data["automobile"] = auto
        except AutomobileVO.DoesNotExist:
            errors["automobile_error"] = "automobile does not exist"

        try:
            salesperson = Salesperson.objects.get(employee_id = data["salesperson"], current_hire=True)
            data["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            errors["salesperson_error"] = "employee_id doesn't exist"

        try:
            customer = Customer.objects.get(id = data["customer"], active=True)
            data["customer"] = customer
        except Customer.DoesNotExist:
            errors["customer_error"] = "customer does not exist"

        if errors:
            return JsonResponse({
                "error":"One or more 404 errors",
                "msg" : {"Errors": errors}
            }, status=404)

        # Check for unexpected extra data ------------------------------------------
        try:
            new_sale = Sale.objects.create(**data)
            return JsonResponse(new_sale, encoder=SaleDetailEncoder, safe=False)

        except TypeError:
            return JsonResponse({
                "error": "Unexpected Properties",
                "msg" : {"Assignable Properties": required_properties}
            }, status = 400)


@require_http_methods(["GET", "DELETE"])
def api_sale(request, sale_id):

    # Check that sale exists before routing to GET or DELETE ------------------------
    try:
        sale = Sale.objects.get(id = sale_id)

    except Sale.DoesNotExist:
        return JsonResponse({
            "error":"Sale id does not exist",
            "msg" : "Sale id does not exist"
        }, status = 404)

    # GET ---------------------------------------------------------------------------
    if request.method == "GET":
        return JsonResponse(sale, encoder=SaleDetailEncoder, safe=False)

    # DELETE ------------------------------------------------------------------------
    else:
        sale.delete()
        return JsonResponse({
            "Deleted":"True",
            "Sale": sale
        }, encoder = SaleDetailEncoder)


# ------------Extra Features---------------

# Endpoint to purge sales data with empty foreignKey reference.
@require_http_methods(["DELETE"])
def sales_purge(request):

    # Yes, if statement is redundant but is there incase require_http_method fails
    if request.method == "DELETE":
        delete_count = Sale.purge_bad_data()
        return JsonResponse({
            "Deleted":f"{delete_count} sale items"
        })
