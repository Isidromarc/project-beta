from django.db import models
from django.db.models import Q

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length = 60, unique=True)
    vin = models.CharField(max_length = 30)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    employee_id = models.CharField(max_length = 30, unique=True)
    current_hire = models.BooleanField(default=True)

    class Meta:
        ordering = ["first_name", "last_name", "employee_id"]

    # --- current_hire -----
    # employee_id set to unique to act as identifier for the salesperson.
    # current_hire attribute bool added as replacement for deletion as
    # we shouldn't delete Salesperson data in order to protect sales history.
    # We can query employees currently on company payroll with
    # current_hire=True while maintaining sales history of previous hires for archive.

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Customer(models.Model):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    address = models.CharField(max_length = 150)
    phone_number = models.CharField(max_length = 15)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ["first_name", "last_name"]

    # ---- active -----
    # Same as salesperson, in order to protect sales data, "active" property
    # is added in order to set it to false in the event of a "delete" request.
    # customer will no longer appear in query results, but the data is retained
    # for the sales.

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    # custom error for bad data during normalization
    class BadInput(Exception):
        pass

    # Normalize phone number to 000-000-0000.
    @classmethod
    def normalize_phone_number(cls, input_phone_number):
        phone_number = ""
        count = 0
        for char in input_phone_number:
            if char.isdigit():
                phone_number += char
                count += 1
                if count == 3 or count == 6:
                    phone_number += "-"

        if len(phone_number) != 12:
            raise cls.BadInput("Expected 10 digit phone number.")

        return phone_number


class Sale(models.Model):
    automobile = models.ForeignKey(
        "AutomobileVO",
        related_name="sales",
        on_delete = models.SET_NULL, # See comment below
        null=True
    )
    salesperson = models.ForeignKey(
        "Salesperson",
        related_name="sales",
        on_delete=models.SET_NULL,
        null=True
    )
    customer = models.ForeignKey(
        "Customer",
        related_name="sales",
        on_delete=models.SET_NULL,
        null=True
    )
    # limit of 10,000,000,000.00 placed on front-end for the obscenely rich.
    price = models.DecimalField(decimal_places=2, max_digits=13)

    # Class method added in order to purge database of any sale data that has
    # an empty foreign key field due to deleted or lost data.
    @classmethod
    def purge_bad_data(cls):
        bad_datas = cls.objects.filter(Q(automobile__isnull = True) | Q(salesperson__isnull = True) | Q(customer__isnull = True))
        for data in bad_datas:
            data.delete()
        return len(bad_datas)


    # All foreign key fields set to SET_NULL on delete as we are interested in
    # protecting sales history in the event that any of the related models are
    # deleted. Related models are not expected to be deleted for any reason,
    # but if they are, setting the property to null will at least preserve some
    # data until a better solution is found. Only way to purposely delete a sale history
    # is by invoking the delete sale endpoint or the purge endpoint.
