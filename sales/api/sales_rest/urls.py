from django.urls import path
from sales_rest.views import (
    api_sales_persons,
    api_sales_person,
    api_customers,
    api_customer,
    api_sales,
    api_sale,
    sales_purge
)

urlpatterns = [
    path("salespeople/", api_sales_persons, name="api_sales_persons"),
    path("salespeople/<int:employee_id>/", api_sales_person, name="api_sales_person"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:customer_id>/", api_customer, name="api_customer"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:sale_id>/", api_sale, name="api_sale"),
    # sales_purge path purposely made obscure until I figure out how to set and check
    # permissions from frontend, and/or add more reliable security to the endpoint.
    path("sales/123AHDON'TUSETHISLINKUNLESSYUNOWATUDOING/2468/", sales_purge, name="forbidden_link")
]
