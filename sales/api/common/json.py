from json import JSONEncoder
from decimal import Decimal
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from datetime import datetime, date
from sales_rest.models import Salesperson, Sale, Customer, AutomobileVO


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime, date)):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

# Added to handle empty ForeingKey references because of deletion
# or lost data.
class NoneTypeEncoder(JSONEncoder):
    def default(self, o):
        if o is None:
            return ""
        else:
            return super().default(o)

# handles decimal.Decimal type data for price attribute in sales.
class DecimalEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return str(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, DecimalEncoder, NoneTypeEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "current_hire"
    ]

    # extra data purely for convenience so frontend won't have to make
    # a separate fetch request for the entire sale data and then filter.
    def get_extra_data(self,o):
        d = []
        for sale in o.sales.all():
            sale_data = {
                "automobile": sale.automobile.vin,
                "customer": sale.customer.__str__(),
                "price": sale.price
            }
            d.append(sale_data)
        return {"sales": d}

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "phone_number",
        "address"
    ]

# Customer Detail Encoder added for future use.
# Even though customer detail and customer list are identical
# more properties might be added to the customer information,
# and having the detail encoder incorporated now will save us from
# having to incorporate it later on a possibly more complex architecture.
class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "phone_number",
        "address"
    ]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
    ]

    # using conditional expressions so server doesnt throw an error in the event
    # that sale object doesn't have an automobile, salesperson or customer model to
    # reference because they were deleted or lost. Other option is to use encoders, but we're
    # trying to keep SalesList data lightweight.
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin if o.automobile else "",
            "salesperson": {
                "name": o.salesperson.__str__(),
                "employee_id": o.salesperson.employee_id,
                "current_hire": o.salesperson.current_hire
            } if o.salesperson else {},
            "customer": o.customer.__str__() if o.customer else ""
        }

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin"
    ]

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile"
    ]

    encoders = {
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerDetailEncoder(),
        "automobile": AutomobileVOEncoder()
    }
